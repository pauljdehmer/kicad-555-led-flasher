EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:555-led-flasher-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x02_Female J1
U 1 1 5A34878E
P 1500 1300
F 0 "J1" H 1500 1400 50  0000 C CNN
F 1 "Conn_01x02_Female" H 1500 1100 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x02_Pitch2.54mm" H 1500 1300 50  0001 C CNN
F 3 "" H 1500 1300 50  0001 C CNN
	1    1500 1300
	1    0    0    -1  
$EndComp
Text GLabel 1300 1300 0    60   Input ~ 0
+9v
Text GLabel 1300 1400 0    60   Input ~ 0
GND
Text GLabel 2750 3150 0    60   Input ~ 0
+9v
$Comp
L R R2
U 1 1 5A348B4F
P 5200 2000
F 0 "R2" V 5280 2000 50  0000 C CNN
F 1 "1K" V 5200 2000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5130 2000 50  0001 C CNN
F 3 "" H 5200 2000 50  0001 C CNN
	1    5200 2000
	-1   0    0    1   
$EndComp
$Comp
L LM555 U1
U 1 1 5A348BCF
P 4200 2150
F 0 "U1" H 3800 2500 50  0000 L CNN
F 1 "LM555" H 4300 2500 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 4200 2150 50  0001 C CNN
F 3 "" H 4200 2150 50  0001 C CNN
	1    4200 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1300 4200 1750
Wire Wire Line
	3500 1300 3500 2350
Wire Wire Line
	3500 2350 3700 2350
Wire Wire Line
	5200 1300 5200 1850
Connection ~ 4200 1300
Wire Wire Line
	5200 2150 4700 2150
$Comp
L R R3
U 1 1 5A348E3A
P 5200 2300
F 0 "R3" V 5280 2300 50  0000 C CNN
F 1 "470K" V 5200 2300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5130 2300 50  0001 C CNN
F 3 "" H 5200 2300 50  0001 C CNN
	1    5200 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3150 5600 3150
Wire Wire Line
	5600 3150 5600 1700
Wire Wire Line
	5600 1700 5200 1700
Connection ~ 5200 1700
Wire Wire Line
	3500 1300 5200 1300
Wire Wire Line
	5200 2450 4700 2450
Wire Wire Line
	4700 2450 4700 2350
Wire Wire Line
	5200 2600 5200 2450
Wire Wire Line
	3300 2600 5200 2600
Wire Wire Line
	3300 2600 3300 1950
Wire Wire Line
	3300 1950 3700 1950
Text GLabel 2700 1450 0    60   Input ~ 0
GND
Wire Wire Line
	4200 2950 4200 2550
$Comp
L C C1
U 1 1 5A348F5C
P 3800 2800
F 0 "C1" H 3825 2900 50  0000 L CNN
F 1 "1uf" H 3825 2700 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P1.50mm" H 3838 2650 50  0001 C CNN
F 3 "" H 3800 2800 50  0001 C CNN
	1    3800 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2600 3800 2650
Connection ~ 3800 2600
$Comp
L LED D1
U 1 1 5A348FE6
P 3150 1450
F 0 "D1" H 3150 1550 50  0000 C CNN
F 1 "LED" H 3150 1350 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 3150 1450 50  0001 C CNN
F 3 "" H 3150 1450 50  0001 C CNN
	1    3150 1450
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5A34911C
P 4700 1700
F 0 "R1" V 4780 1700 50  0000 C CNN
F 1 "1K" V 4700 1700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4630 1700 50  0001 C CNN
F 3 "" H 4700 1700 50  0001 C CNN
	1    4700 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1850 4700 1950
Wire Wire Line
	4700 1550 4700 1450
Wire Wire Line
	2700 2950 4200 2950
Wire Wire Line
	2700 2950 2700 1450
Wire Wire Line
	2700 1450 3000 1450
Wire Wire Line
	4700 1450 3300 1450
Connection ~ 3800 2950
$EndSCHEMATC
